package com.hlabs.publisher;

import java.util.List;

import com.hlabs.event.Event;

public interface Publisher {
	
	// Get publisher Id of the calling publisher object
	public String getPublisherId();
	
	// Publish event e
	public void publishEvent(Event e);
	
	// Unpublish event e
	public void unPublishEvent(Event e);
	
	// get the list of all events published by this publisher
	public List<Event> getPublishedEvents();
}
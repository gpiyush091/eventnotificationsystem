package com.hlabs.publisher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hlabs.event.Event;
import com.hlabs.event.EventManager;

public class PublisherGeneric implements Publisher{
	private String publisherId = null;
	private List<Event> publishedEvents = Collections.synchronizedList(new ArrayList<Event>());
	
	public PublisherGeneric(String publisherId) {
		this.publisherId = publisherId;
		EventManager.getInstance().registerPublisher(this);
	}
	
	public void publishEvent(Event e) {
		EventManager.getInstance().publishEvent(e, this);
		if(!publishedEvents.contains(e))
			publishedEvents.add(e);
		// System.out.println("Event published.");
	}
	
	public void unPublishEvent(Event e) {
		EventManager.getInstance().unpublishEvent(e, this);
		if(publishedEvents.contains(e))
			publishedEvents.remove(e);
		// System.out.println("Event unpublished.");
	}
	
	public String getPublisherId() {
		return publisherId;
	}

	public List<Event> getPublishedEvents() {
		return publishedEvents;
	}

	
}

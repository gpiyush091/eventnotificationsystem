package com.hlabs.restapi;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.hlabs.event.Event;
import com.hlabs.event.EventGeneric;
import com.hlabs.event.EventManager;
import com.hlabs.publisher.Publisher;
import com.hlabs.publisher.PublisherGeneric;
import com.hlabs.subscriber.Subscriber;
import com.hlabs.subscriber.SubscriberGeneric;

// Define REST API to publish events
@Path("/v1")
@SuppressWarnings("unchecked")
public class REST {


	/*
	 * TEST method testing a sequence of steps
	 * 1. Create event
	 * 2. Publish event.
	 * 3. Subscribe to event.
	 * 4. Unsubscribe to event.
	 * 5. Unpublish event.
	 * */
	@GET
	@Path("/test")
	public Response testApi() {
		System.out.println("Simulated test. Contains sleep().");
		try {
			Event e1 = new EventGeneric("e1", 11);
			Event e2 = new EventGeneric("e2", 21);
			Event e3 = new EventGeneric("e3", 31);
			Event e4 = new EventGeneric("e4", 41);
			System.out.println("Events e1 e2 e3 e4 added.");
			Thread.sleep(1000);
			
			Publisher p1 = new PublisherGeneric("p1");
			Publisher p2 = new PublisherGeneric("p2");
			System.out.println("Publishers p1 p2 added.");
			Thread.sleep(1000);
			
			Subscriber s1 = new SubscriberGeneric("s1");
			Subscriber s2 = new SubscriberGeneric("s2");
			System.out.println("Subscribers s1 s2 added.");
			Thread.sleep(1000);
			
			p1.publishEvent(e1);
			p1.publishEvent(e2);
			p1.publishEvent(e4);
			System.out.println("p1 publishes e1, e2, e4");
			
			Thread.sleep(1000);
			
			p2.publishEvent(e3);
			p2.publishEvent(e2);
			System.out.println("p2 publishes e2, e3");
			
			Thread.sleep(3000);

			Subscriber s3 = new SubscriberGeneric("s3");
			System.out.println("Subscriber s3 added");
			
			Thread.sleep(2000);
			
			s1.subscribeEvent(e1);
			Thread.sleep(1000);
			
			s1.subscribeEvent(e2);
			Thread.sleep(1000);
			
			s1.subscribeEvent(e4);
			Thread.sleep(1000);
			System.out.println("s1 subscribed e1, e2, e4");
			
			s2.subscribeEvent(e2);
			Thread.sleep(1000);
			
			s2.subscribeEvent(e3);
			Thread.sleep(1000);
			
			System.out.println("s2 subscribed e2, e4");
			
			s3.subscribeEvent(e4);
			Thread.sleep(1000);
			System.out.println("s3 subscribed e4");
			
			s1.unsubscribeEvent(e1);
			Thread.sleep(1000);
			
			s1.unsubscribeEvent(e2);
			Thread.sleep(1000);
			
			s1.unsubscribeEvent(e4);
			Thread.sleep(1000);
			System.out.println("s3 unsubscribed e1, e2, e4");
			
			s2.unsubscribeEvent(e2);
			Thread.sleep(1000);
			
			s2.unsubscribeEvent(e3);
			Thread.sleep(1000);
			System.out.println("s3 unsubscribed e2, e3");
			
			s3.unsubscribeEvent(e4);
			Thread.sleep(3000);
			System.out.println("s3 unsubscribed e4");
			
			p1.unPublishEvent(e1);
			Thread.sleep(1000);
			
			p1.unPublishEvent(e2);
			Thread.sleep(1000);
			
			p1.unPublishEvent(e4);
			Thread.sleep(1000);
			System.out.println("p1 unpublished e1, e2, e4");
			
			p2.unPublishEvent(e3);
			Thread.sleep(1000);
			
			p2.unPublishEvent(e2);
			Thread.sleep(1000);
			System.out.println("p2 unpublished e3, e2");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return Response.status(200).entity("TEST").build();
	}


	/*********************************** EVENTS ****************************************/

	/*
	 * Get all events registered with the event manager(HUB).
	 * */
	@GET
	@Path("/events")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEventIds() {
		JSONObject response = new JSONObject();
		int status = 400;

		Set<String> events = Collections.synchronizedSet(EventManager.getInstance().getEventsList());

		JSONArray eventIds = new JSONArray();

		for (String eId:events) {
			eventIds.add(eId);
		}

		response.put("eventid", eventIds);
		response.put("success", true);
		status = 200;

		return Response.status(status).entity(response.toJSONString()).build();
	}

	/*********************************** PUBLISHERS ****************************************/

	/*
	 * Get all publishers registered with the event manager(HUB).
	 * */
	@GET
	@Path("/publishers")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPublishers() {
		JSONObject response = new JSONObject();
		int status = 400;

		Set<String> events = Collections.synchronizedSet(EventManager.getInstance().getPublishersList());

		JSONArray eventIds = new JSONArray();

		for (String eId:events) {
			eventIds.add(eId);
		}

		response.put("publisherid", eventIds);
		response.put("success", true);
		status = 200;

		return Response.status(status).entity(response.toJSONString()).build();
	}

	/*
	 * Publish an event
	 * */
	@POST
	@Path("/publisher/{publisherId}/publish")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response publish(
			@PathParam("publisherId") String publisherId,
			@FormParam("eventId") String eId
			) throws ParseException {

		JSONObject response = new JSONObject();
		int status = 400;

		if (publisherId == null) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		publisherId = publisherId.trim();
		if (publisherId.length() == 0) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId == null) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId.length() == 0) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		Publisher p = EventManager.getInstance().publisherRegistered(publisherId);

		if (p == null) {
			p = new PublisherGeneric(publisherId);
		}

		if (p != null) {
			int value = 0;
			Event e = EventManager.getInstance().eventRegistered(eId);
			if (e == null) {
				e = new EventGeneric(eId, value);
			}
			p.publishEvent(e);
		}

		status = 200;
		response.put("message", "Publisher published event");
		return Response.status(status).entity(response.toJSONString()).build();
	}

	/*
	 * Unpublish an event
	 * */
	@POST
	@Path("/publisher/{publisherId}/unpublish")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response unpublish(
			@PathParam("publisherId") String publisherId,
			@FormParam("eventId") String eId
			) throws ParseException {

		JSONObject response = new JSONObject();
		int status = 400;

		if (publisherId == null) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		publisherId = publisherId.trim();
		if (publisherId.length() == 0) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId == null) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId.length() == 0) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		Publisher p = EventManager.getInstance().publisherRegistered(publisherId);

		if (p == null) {
			response.put("message", "No publisher");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (p != null) {
			Event e = EventManager.getInstance().eventRegistered(eId);
			if (e == null) {
				response.put("message", "No such event");
				response.put("success", false);
				return Response.status(status).entity(response.toJSONString()).build();
			}
			p.unPublishEvent(e);
		}

		status = 200;
		response.put("message", "Publisher unpublished event");
		return Response.status(status).entity(response.toJSONString()).build();
	}

	/***************************************** SUBSCRIBER *******************************************************/

	/*
	 * Get all subscribers registered with the event manager(HUB).
	 * */
	@GET
	@Path("/subscribers")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllSubscribers() {
		JSONObject response = new JSONObject();
		int status = 400;

		Set<String> events = Collections.synchronizedSet(EventManager.getInstance().getSubscribersList());

		JSONArray eventIds = new JSONArray();

		for (String eId:events) {
			eventIds.add(eId);
		}

		response.put("subscriberid", eventIds);
		response.put("success", true);
		status = 200;

		return Response.status(status).entity(response.toJSONString()).build();
	}
	
	/*
	 * Subscribe to an event
	 * */
	@POST
	@Path("/subscriber/{subscriberId}/subscribe")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response subscribe(
			@PathParam("subscriberId") String subscriberId,
			@FormParam("eventId") String eId
			) throws ParseException {

		JSONObject response = new JSONObject();
		int status = 400;

		if (subscriberId == null) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		subscriberId = subscriberId.trim();
		if (subscriberId.length() == 0) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId == null) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId.length() == 0) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		Subscriber s = EventManager.getInstance().subscriberRegistered(subscriberId);

		if (s == null) {
			s = new SubscriberGeneric(subscriberId);
		}

		if (s != null) {
			Event e = EventManager.getInstance().eventRegistered(eId);
			if (e == null) {
				response.put("message", "No events");
				response.put("success", false);
				return Response.status(status).entity(response.toJSONString()).build();
			}
			s.subscribeEvent(e);
		}

		status = 200;
		response.put("message", "Subscriber subscribed event");
		return Response.status(status).entity(response.toJSONString()).build();
	}

	/*
	 * Unsubscribe to an event
	 * */
	@POST
	@Path("/subscriber/{subscriberId}/unsubscribe")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response unsubscribe(
			@PathParam("subscriberId") String subscriberId,
			@FormParam("eventId") String eId
			) throws ParseException {

		JSONObject response = new JSONObject();
		int status = 400;

		if (subscriberId == null) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		subscriberId = subscriberId.trim();
		if (subscriberId.length() == 0) {
			response.put("message", "Invalid publisherId");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId == null) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (eId.length() == 0) {
			response.put("message", "No events");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		Subscriber s = EventManager.getInstance().subscriberRegistered(subscriberId);

		if (s == null) {
			response.put("message", "No subscriber");
			response.put("success", false);
			return Response.status(status).entity(response.toJSONString()).build();
		}

		if (s != null) {
			Event e = EventManager.getInstance().eventRegistered(eId);
			if (e == null) {
				response.put("message", "No events");
				response.put("success", false);
				return Response.status(status).entity(response.toJSONString()).build();
			}
			s.unsubscribeEvent(e);
		}

		status = 200;
		response.put("message", "Subscriber unsubscribed event");
		return Response.status(status).entity(response.toJSONString()).build();
	}
}
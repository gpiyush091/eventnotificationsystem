package com.hlabs.restapi;

import java.util.List;

import com.hlabs.event.Event;
import com.hlabs.event.EventGeneric;
import com.hlabs.event.EventManager;
import com.hlabs.publisher.Publisher;
import com.hlabs.publisher.PublisherGeneric;
import com.hlabs.subscriber.Subscriber;
import com.hlabs.subscriber.SubscriberGeneric;

public class Main implements Runnable {
	
	public Event e = null;
	
	public Main(Event e) {
		this.e = e;
	}
	
	@SuppressWarnings("unused")
	// TEST METHOD TO TEST FUNCTIONALITY WITHOUT THE REST API
	public static void main (String[] args) {
		Publisher p1 = new PublisherGeneric("p1");
		Publisher p2 = new PublisherGeneric("p2");
		
		Subscriber s1 = new SubscriberGeneric("s1");
		Subscriber s2 = new SubscriberGeneric("s2");
		
		Event e1 = new EventGeneric("e1", 11);
		Event e2 = new EventGeneric("e2", 21);
		Event e3 = new EventGeneric("e3", 31);
		Event e4 = new EventGeneric("e4", 41);
		
		p1.publishEvent(e1);
		p1.publishEvent(e2);
		p1.publishEvent(e4);
		
		p2.publishEvent(e3);
		p2.publishEvent(e2);
		
		Subscriber s3 = new SubscriberGeneric("s3");
		
		s1.subscribeEvent(e1);
		s1.subscribeEvent(e3);
		s1.subscribeEvent(e4);
		
		s2.subscribeEvent(e2);
		s2.subscribeEvent(e3);
		
		s3.subscribeEvent(e4);
		
		
		List<Publisher> pubs = EventManager.getInstance().listPublishersofEvent(e1);
		pubs = EventManager.getInstance().listPublishersofEvent(e2);
		pubs = EventManager.getInstance().listPublishersofEvent(e3);
		pubs = EventManager.getInstance().listPublishersofEvent(e4);
		
		
		List<Subscriber> subs = EventManager.getInstance().listSubscribersofEvent(e1);
		subs = EventManager.getInstance().listSubscribersofEvent(e2);
		subs = EventManager.getInstance().listSubscribersofEvent(e3);
		subs = EventManager.getInstance().listSubscribersofEvent(e4);
		
		
		new Thread(new Main(e1)).start();
		new Thread(new Main(e2)).start();
		new Thread(new Main(e3)).start();
		new Thread(new Main(e4)).start();
		
	}

	final int TIMES = 10;
	
	public void run() {
		for (int i=0; i<=TIMES; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int value = (int) (Math.random()*10.0f);
			e.updateEvent(value);
		}
	}
}

package com.hlabs.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
public class ContextListener implements ServletContextListener {

	/**
	 * Default constructor. 
	 */
	public ContextListener() {
	}
	
	private static ServletContextEvent context = null;

	/*
	 * Destroy context and stop all remaining user created threads by setting context variable to null.
	 * */
	public void contextDestroyed(ServletContextEvent cxt) {
		context = null;
		try {
			Thread.sleep(5000); // Provided so that threads can query the condition (context == null) and stop
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static ServletContextEvent getServletContextEvent() {
		return context;
	}

	public void contextInitialized(ServletContextEvent cxt) {
		context = cxt;
	}
}

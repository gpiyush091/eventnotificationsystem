package com.hlabs.subscriber;

import java.util.List;

import com.hlabs.event.Event;

public interface Subscriber {
	
	public String getsubscriberId();
	
	public void subscribeEvent(Event e);
	public void unsubscribeEvent(Event e);
	
	public void pingUpdateEvent(Event eId);
	public void pingNewEvent(Event eId);
	
	public List<Event> getSubscribedEvents();
	
	public void unregisterSubscriber();
}

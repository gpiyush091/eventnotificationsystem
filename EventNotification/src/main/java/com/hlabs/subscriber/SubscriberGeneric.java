package com.hlabs.subscriber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hlabs.event.Event;
import com.hlabs.event.EventManager;

public class SubscriberGeneric implements Subscriber{
	private String subscriberId = null;
	private List<Event> subscribedEvents = Collections.synchronizedList(new ArrayList<Event>());
	
	public SubscriberGeneric (String subscriberId) {
		this.subscriberId = subscriberId;
		EventManager.getInstance().registerSubscriber(this);
		// System.out.println("Subscriber: "+subscriberId+" created and registered with hub.");
	}
	
	public String getsubscriberId() {
		return subscriberId;
	}
	
	public void subscribeEvent(Event e) {
		boolean flag = EventManager.getInstance().subscribe(e, this);
		if (flag) {
			if(!subscribedEvents.contains(e))
				subscribedEvents.add(e);
		}
		// System.out.println("Subscriber: "+subscriberId+" subscribed to event: "+e.getEventId()+".");
	}
	
	public void unsubscribeEvent(Event e) {
		boolean flag = EventManager.getInstance().unsubscribe(e, this);
		if (flag) {
			if(subscribedEvents.contains(e))
				subscribedEvents.remove(e);	
		}
		// System.out.println("Subscriber: "+subscriberId+" unsubscribed to event: "+e.getEventId()+".");
	}
	
	public void pingUpdateEvent(Event e) {
		System.out.println("Event updated. Notifying subscriber: "+subscriberId);
	}
	
	public void pingNewEvent(Event e) {
		System.out.println("Event published. Notifying subscriber: "+subscriberId);
	}
	
	public List<Event> getSubscribedEvents() {
		return subscribedEvents;
	}

	public void unregisterSubscriber() {
		EventManager.getInstance().unregisterSubscriber(this);
	}
}

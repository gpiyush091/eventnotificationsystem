package com.hlabs.event;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hlabs.publisher.Publisher;
import com.hlabs.subscriber.Subscriber;

public class EventManager {
	private Map<String, Event> events = Collections.synchronizedMap(new HashMap<String, Event>());
	private Map<String, Subscriber> subscribers = Collections.synchronizedMap(new HashMap<String, Subscriber>());
	private Map<String, Publisher> publishers = Collections.synchronizedMap(new HashMap<String, Publisher>());
	
	private static EventManager _instance = null;

	public static synchronized EventManager getInstance() {
		if (_instance == null)
			_instance = new EventManager();
		return _instance;
	}

	/****************************************** Publisher Methods ******************************************/
	/*
	 * Check whether the publisher is already registered
	 * @param  
	 * 		pId - publisher id
	 * @return 
	 * 		Publisher object if publisher already present
	 * 		null if publisher not present
	 * */
	public Publisher publisherRegistered(String pId) {
		if (publishers.containsKey(pId)) return publishers.get(pId);
		return null;
	}
	
	/*
	 * Register Publisher to the HUB. Called when publisher is first created
	 * @Param
	 * 		Publisher Object
	 * */
	public void registerPublisher(Publisher pub) {
		if (publisherRegistered(pub.getPublisherId()) == null) {
			publishers.put(pub.getPublisherId(), pub);
		}
	}
	
	/*
	 * Unregister publisher.
	 * @Param
	 * 		Publisher Object
	 * */
	public void unregisterPublisher(Publisher pub) {
		if (publisherRegistered(pub.getPublisherId()) != null) {
			List<Event> pubEvents = Collections.synchronizedList(pub.getPublishedEvents());
			synchronized (pubEvents) {
				for (int i=0; i<pubEvents.size(); i++) {
					pubEvents.get(i).removePublisher(pub);
				}
			}
			publishers.remove(pub.getPublisherId());
		}
	}
	
	/*
	 * Publish event
	 * @Param
	 * 		Event Object
	 * 		Publisher Object
	 * */
	public void publishEvent(Event e, Publisher pub) {
		if (!events.containsKey(e.getEventId())) {
			registerEvent(e, pub);
		} else {
			if (!e.getPublishers().contains(pub)) {
				e.addPublisher(pub);
				// Notify every subscriber
				Set<String> subs = Collections.synchronizedSet(subscribers.keySet());
				synchronized (subs) {
					for (String sId:subs) {
						subscribers.get(sId).pingNewEvent(e);
					}
				}
			}
		}
	}

	/*
	 * Unpublish event
	 * @Param
	 * 		Event Object
	 * 		Publisher Object
	 * */
	public void unpublishEvent(Event e, Publisher pub) {
		if (events.containsKey(e.getEventId())) {
			if (e.getPublishers().contains(pub)) {
				e.removePublisher(pub);
				List<Subscriber> subs = Collections.synchronizedList(e.getSubscribers());
				synchronized (subs) {
					for (int i=0; i<subs.size(); i++) {
						e.removeSubscriber(subs.get(i));	
					}
				}
				unregisterEvent(e);
			}
		}
	}
	
	/*
	 * Get the list of all publishers registered
	 * @return
	 * 		Set of publishers ids
	 * */
	public Set<String> getPublishersList() {
		return publishers.keySet();
	}

	/****************************************** Subscriber methods ******************************************/
	
	/*
	 * Check for subscriber already registered
	 * @param
	 * 		sId - subscriber id
	 * @return
	 * 		Subscriber object
	 * */
	public Subscriber subscriberRegistered(String sId) {
		if (subscribers.containsKey(sId)) return subscribers.get(sId);
		return null;
	}
	
	/*
	 * Register subscriber
	 * @param
	 * 		Subscriber Object
	 * */
	public void registerSubscriber(Subscriber s) {
		if (!subscribers.containsKey(s.getsubscriberId())) {
			subscribers.put(s.getsubscriberId(), s);
			// Send notification of already added events to the new subscriber
			for (Event e:events.values()) {
				s.pingNewEvent(e);
			}
		}
	}

	/*
	 * Unregister subscriber
	 * @param
	 * 		Subscriber object
	 * */
	public void unregisterSubscriber(Subscriber s) {
		if (subscribers.containsKey(s.getsubscriberId())) {
			List<Event> subEvents = Collections.synchronizedList(s.getSubscribedEvents());
			synchronized (subEvents) {
				for (int i=0; i<subEvents.size(); i++) {
					subEvents.get(i).removeSubscriber(s);
				}
			}
			subscribers.remove(s.getsubscriberId());
			return;
		}
	}

	/*
	 * Subscribe to an event
	 * @param
	 * 		Event object
	 * 		Subscriber object
	 * @return
	 * 		true if subscribed
	 * 		false if event does not exist - event not registered
	 * */
	public boolean subscribe(Event e, Subscriber s) {
		if (events.containsKey(e.getEventId())) {
			if (e.getPublishers().size()>0) { 
				if (!e.getSubscribers().contains(s))
					e.addSubscriber(s);
				return true;
			}
		}
		return false;
	}

	/*
	 * Unsubscribe to an event
	 * @param
	 * 		Event object
	 * 		Subscriber object
	 * @return
	 * 		true if unsubscribed
	 * 		false if event does not exist. Event not registered in HUB
	 * */
	public boolean unsubscribe(Event e, Subscriber s) {
		if (events.containsKey(e.getEventId())) {
			if (e.getSubscribers().contains(s))
				e.removeSubscriber(s);
			return true;
		}
		return false;
	}

	/*
	 * Get the set of all subscribers
	 * @return
	 * 		Set of all subscriber ids registered to the HUB. 
	 * */
	public Set<String> getSubscribersList() {
		return subscribers.keySet();
	}
	
	/****************************************** Events methods ******************************************/
	
	/*
	 * Check for Event registered to the hub
	 * @param
	 * 		event id
	 * @return
	 * 		Event object
	 * */
	public Event eventRegistered(String eId) {
		if (events.containsKey(eId)) return events.get(eId);
		return null;
	}
	
	/*
	 * Register event to HUB. Called on first publish of the event.
	 * @param
	 * 		Event object
	 * 		publisher object
	 * */
	private void registerEvent(Event e, Publisher pub) {
		if (!events.containsKey(e.getEventId())) {
			events.put(e.getEventId(), e);
			if (pub != null) {
				if (!e.getPublishers().contains(pub)) {
					e.addPublisher(pub);
					// Notify every subscriber
					Set<String> subs = Collections.synchronizedSet(subscribers.keySet());
					synchronized (subs) {
						for (String sId:subs) {
							subscribers.get(sId).pingNewEvent(e);
						}
					}
				}
			}
			new Thread(new EventUpdateService(e)).start();
		}
	}

	/*
	 * Unregister event from the HUB. Remove subscriber and publisher 
	 * remove event from their corresponding published events and subscriber lists
	 * @param
	 * 		Event object
	 * */
	private void unregisterEvent(Event e) {
		if (events.containsKey(e.getEventId())) {
			if (e.getPublishers().size() == 0) {
				// get list of publishers and remove event from there
				List<Publisher> pubs = Collections.synchronizedList(e.getPublishers());
				List<Subscriber> subs = Collections.synchronizedList(e.getSubscribers());
				synchronized (pubs) {
					for (int i=0; i<pubs.size(); i++) {
						unpublishEvent(e, pubs.get(i));
					}
				}
				synchronized (subs) {
					for (int i=0; i<subs.size(); i++) {
						unsubscribe(e, subs.get(i));
					}	
				}
				events.remove(e.getEventId());
			}
		}
	}

	/*
	 * list subscribers of the event
	 * @param
	 * 		Event object
	 * @return
	 * 		subscribers of a particular event if event exists
	 * 		else null
	 * */
	public List<Subscriber> listSubscribersofEvent(Event e) {
		if (events.containsKey(e.getEventId()))
			return e.getSubscribers();
		return null;
	}

	/*
	 * list publishers of the event
	 * @param
	 * 		Event object
	 * @return
	 * 		publishers of a particular event if event exists
	 * 		else null
	 * */
	public List<Publisher> listPublishersofEvent(Event e) {
		if (events.containsKey(e.getEventId()))
			return e.getPublishers();
		return null;
	}

	/*
	 * Get event object given event id
	 * @param
	 * 		event id
	 * @return
	 * 		event object if it exists
	 * 		else null
	 * */
	public Event getEvent(String eId) {
		if (events.containsKey(eId)) {
			return events.get(eId);
		}
		return null;
	}

	/*
	 * Update event object
	 * @param
	 * 		event object 
	 * 		new value of the event
	 * */
	public void updateEvent(Event e, int newVal) {
		if (events.containsKey(e.getEventId())) {
			e.updateEvent(newVal);
			List<Subscriber> subscribers = Collections.synchronizedList(e.getSubscribers());
			synchronized (subscribers) {
				for (int i=0; i<subscribers.size(); i++) {
					subscribers.get(i).pingUpdateEvent(e);
				}	
			}
			return;
		}
	}
	
	/*
	 * Get the set of registered events
	 * @return
	 * 		set of registered event ids.
	 * */
	public Set<String> getEventsList() {
		return events.keySet();
	}
}
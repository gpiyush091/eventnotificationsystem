package com.hlabs.event;

import java.util.List;

import com.hlabs.publisher.Publisher;
import com.hlabs.subscriber.Subscriber;

public interface Event {
	
	// Unique event id
	public String getEventId();
	
	// update event every second once the event is published
	public void updateEvent(int value);
	
	// retrieve value for the event
	public int getValue();
	
	// add publisher for the event
	public void addPublisher(Publisher p);
	
	// remove publisher for the event
	public void removePublisher(Publisher p);
	
	// add subscriber for the event
	public void addSubscriber(Subscriber s);
	
	// remove subscriber for the event
	public void removeSubscriber(Subscriber s);
	
	// get all publishers of the event
	public List<Publisher> getPublishers();
	
	// get all subscribers of the event
	public List<Subscriber> getSubscribers();

	// define the state of the event
	public Boolean getState();
}

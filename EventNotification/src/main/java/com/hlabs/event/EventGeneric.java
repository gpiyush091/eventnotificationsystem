package com.hlabs.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hlabs.publisher.Publisher;
import com.hlabs.subscriber.Subscriber;

public class EventGeneric implements Event {
	private String eId;
	private int value;
	private boolean state = true;
	
	List<Subscriber> subscribers = Collections.synchronizedList(new ArrayList<Subscriber>());
	List<Publisher> publishers = Collections.synchronizedList(new ArrayList<Publisher>());
	
	public EventGeneric(String eId, int value) {
		this.eId = eId;
		this.value = value;
		// System.out.println("Event: "+eId+" created.");
	}
	
	public String getEventId() {
		return eId;
	}
	
	public int getValue() {
		return value;
	}
	
	public void updateEvent(int value) {
		this.value = value;
		if (publishers.size()>0) {
			for (int i=0; i<subscribers.size(); i++) {
				subscribers.get(i).pingUpdateEvent(this);
			}
		}
	}

	public void addPublisher(Publisher p) {
		if (p!=null)
			if (!publishers.contains(p))
				publishers.add(p);
	}

	public void removePublisher(Publisher p) {
		if (p!=null) {
			if (publishers.contains(p))
				publishers.remove(p);
			if (publishers.size() == 0)
				state = true;
		}
	}

	public void addSubscriber(Subscriber s) {
		if (s!=null)
			if (!subscribers.contains(s))
				subscribers.add(s);
	}

	public void removeSubscriber(Subscriber s) {
		if (s!=null)
			if (subscribers.contains(s))
				subscribers.remove(s);
	}

	public List<Publisher> getPublishers() {
		return publishers;
	}

	public List<Subscriber> getSubscribers() {
		return subscribers;
	}

	public Boolean getState() {
		return state;
	}
}

package com.hlabs.event;

import com.hlabs.listeners.ContextListener;

public class EventUpdateService implements Runnable {

	private Event e = null;

	public EventUpdateService(Event e) {
		this.e = e;
	}

	/*
	 * Update event every 1 sec.
	 * Stop thread when application closes => context becomes null
	 * or, when event is unregistered or unpublished (e.getState() == false)
	 * */
	public void run() {
		while (ContextListener.getServletContextEvent() != null && e.getState()) {
			int value = (int) (Math.random()*100.0f);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			e.updateEvent(value);
		}
	}
}

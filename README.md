1a. 3 entities - Publisher, Subscriber and Events

1b. EventManager Singleton that controls flow between publishers, events and subscribers. It acts as a "HUB" for managing all communication between the 3 entities.

2a. Publisher publishes events through EventManager.

2b. Subscriber subscribes to events through EventManager.

2c. Events registered with EventManager on creation.

2d. When Events destroyed then unregister from program manager.

2e. Newly registered events are notified to all subscribers.

2f. Updated events are notified to subscribers subscribing them.

3. Created a main function. The class containing main function com.hlabs.restapi.Main implements Runnable and defines a run method that is used to change the value of events periodically.

4. Publisher interface exposes following APIs:
		
                public String getPublisherId();
	        public void publishEvent(Event e);	
	        public void unPublishEvent(Event e);


5. Created a PublisherGeneric class implements Publisher.

6. Subscriber interface exposes the following APIs:
		
                public String getsubscriberId();
	
	        public void subscribeEvent(Event e);
           	public void unsubscribeEvent(Event e);
	
	        public void pingUpdateEvent(Event eId);
	        public void pingNewEvent(Event eId);
	
	        public List<Event> getSubscribedEvents();
	
	        public void unregisterSubscriber();

7. Created a SubscriberGeneric class implements Subscriber.

8. Event Interface exposes:
		
                public String getEventId();
	        public void updateEvent(int value);
	        public int getValue();
	
	        public void addPublisher(Publisher p);
	        public void removePublisher(Publisher p);
	
	        public void addSubscriber(Subscriber s);
	        public void removeSubscriber(Subscriber s);
	
	        public List<Publisher> getPublishers();
	
	        public List<Subscriber> getSubscribers();

	        public Boolean getState();

9. EventGeneric implements Event

10. Created a class for REST API implemention for publishing events. The REST API is Jersey based.

11. ServletContextListener to define events, subscribers before hand.

12. Start event thread on first publish of that event.

13. event updation will be handled by a EventUpdateService class.

14. Stop event thread on last unpublish of event.

15. REST API implementation for publishing events. 

16. Commenting

17. REST API Usage - (All post calls are x-www-form-urlencoded)

*  Base url: http://localhost:8080/EventNotification/api/v1
                       
*  Automated test: GET Call to http://localhost:8080/EventNotification/api/v1/test
                       
PUBLISH: 

*                POST call

*                http://localhost:8080/EventNotification/api/v1/publisher/{publisherId}/publish

*                Param:                key = eventId               value = e1

UNPUBLISH: 

*                POST call

*                http://localhost:8080/EventNotification/api/v1/publisher/{publisherId}/unpublish

*                Param:               key = eventId               value = e1

SUBSCRIBE: 

*                POST call

*                http://localhost:8080/EventNotification/api/v1/subscriber/{subscriberId}/subscribe

*                Param:               key = eventId               value = e1

UNSUBSCRIBE: 

*                POST call

*                http://localhost:8080/EventNotification/api/v1/subscriber/{subscriberId}/unsubscribe

*                Param:               key = eventId               value = e1

ALL PUBLISHERS:

*                GET CALL

*                http://localhost:8080/EventNotification/api/v1/publishers

ALL SUBSCRIBERS:

*                GET CALL

*                http://localhost:8080/EventNotification/api/v1/subscribers

ALL EVENTS:

*                GET CALL

*                http://localhost:8080/EventNotification/api/v1/events

Note- This is a maven web project. Import it in Eclipse and add the project in Tomcat server and then start the server to run.

[REFERENCES]

1. Observer Pattern in Java - https://www.youtube.com/watch?v=wiQdrH2YpT4

2. Google pubsubhubbub - https://www.youtube.com/watch?v=B5kHx0rGkec